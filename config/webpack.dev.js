const autoprefixer = require("autoprefixer");
const eslintFormatter = require("react-dev-utils/eslintFormatter");
const flexbugsFixes = require("postcss-flexbugs-fixes");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

// Utils
const envs = require("./env");
const paths = require("./paths");

// Plugins
const definePlugin = new webpack.DefinePlugin(envs);
const htmlWebpackPlugin = new HtmlWebpackPlugin({
  inject: true,
  template: paths.appHtml
});
const hotModuleReplacementPlugin = new webpack.HotModuleReplacementPlugin();

module.exports = {
  mode: "development",
  devtool: "cheap-module-eval-source-map",
  entry: paths.appIndexJs,
  output: {
    path: paths.appPublic,
    publicPath: "/",
    filename: "[name].js",
    chunkFilename: "[name].chunk.js"
  },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        include: paths.appSrc,
        use: [
          {
            loader: "eslint-loader",
            options: {
              formatter: eslintFormatter,
              eslintPath: "eslint"
            }
          }
        ]
      },
      {
        test: /\.(js|jsx)$/,
        include: paths.appSrc,
        use: ["babel-loader"]
      },
      {
        test: /\.css$/,
        include: paths.appSrc,
        use: [
          { loader: "style-loader" },
          {
            loader: "css-loader"
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: () => [
                flexbugsFixes,
                autoprefixer({
                  browsers: ["> 1%", "last 2 versions"]
                })
              ]
            }
          }
        ]
      },
      {
        test: /\.(eot|svg|otf|ttf|woff|woff2)$/,
        use: "file-loader"
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: [
          "file-loader",
          {
            loader: "image-webpack-loader",
            options: {
              name: "[path][name]-[hash:8].[ext]",
              query: {
                gifsicle: {
                  interlaced: true
                },
                mozjpeg: {
                  progressive: true
                },
                optipng: {
                  optimizationLevel: 7
                },
                pngquant: {
                  quality: "65-90",
                  speed: 4
                }
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [definePlugin, htmlWebpackPlugin, hotModuleReplacementPlugin],
  devServer: {
    contentBase: paths.appPublic,
    historyApiFallback: true,
    hot: true,
    port: 3001,
    watchContentBase: true
  }
};
