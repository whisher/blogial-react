import React from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from "@material-ui/core";

const styles = theme => ({
  button: {
    display: "block",
    width: "100%",
    height: "3rem",
    color: theme.colors.white,
  }
});

const LoaderButton = (props) => {
  const {
    children,
    classes,
    loading,
    ...rest
  } = props;
  return (
    <Button {...rest} className={classes.button}>
      {loading ? <CircularProgress style={{ color: 'white' }} size={20}/> : children}
    </Button>
  )
};

export default withStyles(styles)(LoaderButton);