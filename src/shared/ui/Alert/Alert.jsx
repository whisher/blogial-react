import React from 'react';
import { withStyles } from "@material-ui/core";

const style = {
  position: "relative",
  padding: "1rem 1.25rem",
  marginBottom: "1rem",
  borderRadius: ".25rem",
};

const styles = () => ({
  alert: {
    backgroundColor: "#ffebee",
    border: "1px solid #c62828",
    color: "#c62828"
  }
});

const Alert = (props) => {
  const {
    children,
    classes
  } = props;
  return (
    <div style={style} className={classes.alert} role="alert">
      {children}
    </div>
  )
};

export default withStyles(styles)(Alert);