import React, { Component } from "react";

import { withStyles } from "@material-ui/core";


const styles = () => ({
  container:{
    display: 'flex'
  }
});

class BlogHome extends Component {
  state = {};
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
       Home       
      </div>
    );
  }
}

export default withStyles(styles)(BlogHome);
