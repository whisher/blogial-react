import React from "react";

import { Link } from "react-router-dom";

// Styles
import { withStyles } from "@material-ui/core";

import { AUTH_LOGIN, ADMIN_DASHBOARD } from "../../../../routes";

const styles = () => ({
  nav: {
    flex: "1 0 auto"
  },
  ul: {
    display: "flex",
    '& li':{
      marginLeft: '1rem'
    }
  }
  
});

const BlogLayoutNav = ({ classes }) => (
  <nav className={classes.nav}>
    <ul className={classes.ul}>
      <li>
        <Link to="/">
          Home
        </Link>
      </li>
      <li>
        <Link to="/">
          About
        </Link>
      </li>
      <li>
        <Link to="/">
          Contact
        </Link>
      </li>
      <li>
        <Link to={ADMIN_DASHBOARD}>
          Admin
        </Link>
      </li>
      <li>
        <Link to={AUTH_LOGIN}>
          Login
        </Link>
      </li>
    </ul>
  </nav>
);

export default withStyles(styles)(BlogLayoutNav);
