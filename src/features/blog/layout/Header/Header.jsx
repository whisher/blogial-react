import React from "react";

import BlogLayoutBrand from '../Brand/Brand';
import BlogLayoutNav from '../Nav/Nav';

// Styles
import { withStyles } from "@material-ui/core";

const styles = () => ({
  header: {
    display: "flex"
  }
});

const BlogLayoutHeader = ({ classes }) => (
  <header className={classes.header}>
    <BlogLayoutBrand />
    <BlogLayoutNav />
  </header>
);

export default withStyles(styles)(BlogLayoutHeader);
