// Core
import React from "react";

import { Route } from "react-router-dom";
import { withStyles } from "@material-ui/core";


import { HOME } from "../../../../routes";

// Layout
import BlogLayoutFooter from "../Footer/Footer";
import BlogLayoutHeader from "../Header/Header";

import BlogHome from '../../Home/Home';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.colors.white
  },
  main:{
    flex: '1 0 auto'
  }
});

const BlogLayoutMain = ({ classes }) => {
  return (
    <div className={classes.container}>
      <BlogLayoutHeader/>
      <main className={classes.container}>
        <Route path={HOME} component={BlogHome} />
      </main>
      <BlogLayoutFooter />
    </div>
  );
};

export default withStyles(styles)(BlogLayoutMain);
