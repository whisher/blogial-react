// Core
import React, {Component} from "react";
import { connect } from 'react-redux';
import { Route, Redirect } from "react-router-dom";
import { withStyles } from "@material-ui/core";

import { getAccount } from '../../../../ducks/account';

// Layout
import AdminLayoutFooter from "../Footer/Footer";
import AdminLayoutHeader from "../Header/Header";

// Routes
import { 
  ADMIN_DASHBOARD, 
  AUTH_LOGIN, 
  ADMIN_POSTS,
  ADMIN_POSTS_POST,
  ADMIN_POSTS_POST_ID} from "../../../../routes";
import AdminDashboardMain from '../../dashboard/Main/Main';
import AdminPostsMain from '../../posts/Main/Main';
import AdminPostsPage from '../../posts/Page/Page';
const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    backgroundColor: theme.colors.white
  },
  main:{
    flex: "1 0 auto",
    display: "flex",
    justifyContent: "center"
  },
  content:theme.layout.admin.container
});
class AdminLayoutMain extends Component {
  
  componentDidMount() {
    this.props.getAccount();
  }
  render(){ 
    let authRedirect = null;
    if (!this.props.loaded && this.props.loading) {
      return  <div>LOADING....</div>;
    }
    if (!this.props.account ) {
      authRedirect = <Redirect to={AUTH_LOGIN} />;
    }
    const { classes } = this.props;
    return (
      <React.Fragment>
        {authRedirect}
        <div className={classes.container}>
          <AdminLayoutHeader/>
          <main className={classes.main}>
            <div className={classes.content}>
              <Route path={ADMIN_DASHBOARD} component={AdminDashboardMain} />
              <Route path={ADMIN_POSTS_POST} exact component={AdminPostsPage} />
              <Route path={ADMIN_POSTS_POST_ID} component={AdminPostsPage} />
              <Route path={ADMIN_POSTS} component={AdminPostsMain} />
            </div>
          </main>
          <AdminLayoutFooter />
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    account: state.account.account,
    error: state.account.error,
    loaded: state.account.loaded,
    loading: state.account.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAccount: ( ) => dispatch(getAccount ())
  };
};


export default connect( mapStateToProps, mapDispatchToProps )(withStyles(styles)(AdminLayoutMain));
