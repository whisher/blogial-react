import React from "react";

// Styles
import { withStyles } from "@material-ui/core";

import AdminLayoutBrand from '../Brand/Brand';
import AdminLayoutNavbar from '../Navbar/Navbar';

const styles = theme => ({
  container: {
    display: "flex",
    alignItems: "center",
    height: "100%",
    width: "100%",
    borderBottom: `1px solid ${theme.colors.gray}`
  },
  brand:{},
  navbar:{
    flex: "1 0 auto",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center", 
  }
});

const AdminLayoutMasthead = ({ classes }) => {
  return (
    <div className={classes.container}>
      <div className={classes.brand}>
        <AdminLayoutBrand />
      </div>
      <div className={classes.navbar}>
        <AdminLayoutNavbar />
      </div>
    </div>
  );
};

export default withStyles(styles)(AdminLayoutMasthead);
