import React from "react";
import { Link } from "react-router-dom";

// Styles
import { withStyles } from "@material-ui/core";

import logo from "../../../../assets/images/logo.png";

const styles = () => ({
  h1: {
    "& a": {
      display: "block"
    },
    "& img": {
      width: "5rem"
    }
  }
});

const AdminLayoutBrand = ({ classes }) => (
  <h1 className={classes.h1}>
    <Link to="/">
      <img src={logo} alt="logo" />
    </Link>
  </h1>
);

export default withStyles(styles)(AdminLayoutBrand);
