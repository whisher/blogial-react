import React, {Component} from "react";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";

import { ADMIN_DASHBOARD, ADMIN_POSTS } from "../../../../routes";

const styles = () => ({
  nav: {
    display: "flex",
    alignItems: "center",
    flex: "1 0 auto",
    height: "100%",
    width: "100%"
  },
  ul: {
    display: "flex",
    "& li":{
      marginLeft: "1rem"
    },
    "& a":{
      textTransform: "uppercase",
      fontSize: "110%",
      fontWeight: 500
    }
  }
});

class AdminLayoutNav extends Component {
  render() {
    const { classes } = this.props;
    return (
      <nav className={classes.nav}>
        <ul className={classes.ul}>
          <li>
            <Link to={ADMIN_DASHBOARD}>
              Dashboard
            </Link>
          </li>
          <li>
            <Link to={ADMIN_POSTS}>
              Posts
            </Link>
          </li>
          <li>
            <Link to="/">
              Users
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default withStyles(styles)(AdminLayoutNav);
