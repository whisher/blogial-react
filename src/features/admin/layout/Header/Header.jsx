import React from "react";

import AdminLayoutMasthead from '../Masthead/Masthead';
import AdminLayoutNav from '../Nav/Nav';

// Styles
import { withStyles } from "@material-ui/core";

const styles = theme => ({
  header: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%"
  },
  container: theme.layout.admin.container,
  masthead: theme.layout.admin.masthead,
  nav: theme.layout.admin.nav
});

const AdminLayoutHeader = ({ classes }) => (
  <header className={classes.header}>
    <div className={classes.container}>
      <div className={classes.masthead}>
        <AdminLayoutMasthead />
      </div>
      <div className={classes.nav}>
        <AdminLayoutNav />
      </div>
    </div>
  </header>
);

export default withStyles(styles)(AdminLayoutHeader);
