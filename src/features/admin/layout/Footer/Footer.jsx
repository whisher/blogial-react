import React from "react";

// Styles
import { withStyles } from "@material-ui/core";

const styles = () => ({
  footer: {
    display: "flex",
    justifyContent: "center"
  }
});

const AdminLayoutFooter = ({ classes }) => {
  return (
    <footer className={classes.footer}>
      <p>
        &#169;Blogial - 2018
      </p>
    </footer>
  );
};

export default withStyles(styles)(AdminLayoutFooter);
