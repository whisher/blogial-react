import React, {Component} from "react";

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { withStyles } from "@material-ui/core";

const styles = () => ({
  container: {
    display: "flex",
    alignItems: "center",
    height: "100%"
  },
  ul: {
    display: "flex",
    '& li':{
      marginLeft: '1rem'
    }
  }
});
class AdminLayoutNavbar extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const { classes } = this.props;
    return (
      <ul className={classes.ul}>
        <li>
          <NotificationsIcon />
        </li>
        <li>
          <div>
            <Button
                aria-owns={anchorEl ? 'simple-menu' : null}
                aria-haspopup="true"
                onClick={this.handleClick}>
                whisher
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}>
              <MenuItem onClick={this.handleClose}>Profile</MenuItem>
              <MenuItem onClick={this.handleClose}>My account</MenuItem>
              <MenuItem onClick={this.handleClose}>Logout</MenuItem>
            </Menu>
          </div>
        </li>
      </ul>
    );
  }
}

export default withStyles(styles)(AdminLayoutNavbar);
