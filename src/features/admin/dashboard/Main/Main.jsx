import React, { Component } from "react";

import { withStyles } from "@material-ui/core";


const styles = () => ({
  container:{
    display: 'flex'
  }
});

class AdminDashboardMain extends Component {
  state = {};
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
      AdminDashboard   
      </div>
    );
  }
}

export default withStyles(styles)(AdminDashboardMain);
