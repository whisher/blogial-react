import React, {Component} from "react";
import { withStyles } from "@material-ui/core";

import AdminPostsForm from '../Form/Form';

const styles = theme => ({
  container: {
    backgroundColor: theme.colors.white
  }
});
class AdminPostsPage extends Component{

  render() {
    console.log('roue',this.props.match.params);
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <AdminPostsForm />
      </div>
    );
  }
}

export default withStyles(styles)(AdminPostsPage);

