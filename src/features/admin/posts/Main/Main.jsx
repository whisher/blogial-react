import React, {Component} from "react";
import { Link } from "react-router-dom";

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from "@material-ui/core";

import { ADMIN_POSTS_POST } from "../../../../routes";

const styles = theme => ({
  container: {
    backgroundColor: theme.colors.white
  },
  navbar:{
    display: "flex",
    justifyContent: "center"
  }
});
class AdminPostsMain extends Component{

  render() {
    
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <div className={classes.navbar}>
          <Link to={`${ADMIN_POSTS_POST}/3`}>
            <Button variant="fab" color="secondary" aria-label="Add">
              <AddIcon />
            </Button>
          </Link>
        </div> 
      </div>
    );
  }
}

export default withStyles(styles)(AdminPostsMain);

