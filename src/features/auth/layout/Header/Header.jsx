import React from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

// Styles
import { withStyles } from "@material-ui/core";

import logo from "../../../../assets/images/logo.png";

const styles = () => ({
  header: {
    display: "flex",
    justifyContent: "center"
  },
  h1: {
    display: "flex",
    flexDirection: "column",
    "& a": {
      display: "block"
    },
    "& img": {
      maxWidth: "10rem"
    }
  }
});

const AuthLayoutHeader = ({ classes }) => (
  <header className={classes.header}>
    <Typography variant="h1" className={classes.h1}>
      <Link to="/">
        <img src={logo} alt="logo" />
      </Link>
      <Typography variant="body2" align="center" color="textSecondary">
        Architects your <strong>blogial</strong>
      </Typography>
    </Typography>
  </header>
);

export default withStyles(styles)(AuthLayoutHeader);
