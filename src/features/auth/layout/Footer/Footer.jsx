import React from "react";

import Typography from "@material-ui/core/Typography";
// Styles
import { withStyles } from "@material-ui/core";

const styles = () => ({
  footer: {
    display: "flex",
    justifyContent: "center"
  }
});

const AuthLayoutFooter = ({ classes }) => {
  return (
    <footer className={classes.footer}>
      <Typography
        variant="body1"
        align="center"
        color="textSecondary"
        gutterBottom
      >
        &#169;Blogial - 2018
      </Typography>
    </footer>
  );
};

export default withStyles(styles)(AuthLayoutFooter);
