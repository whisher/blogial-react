// Core
import React, {Component} from "react";
import { Route, Redirect } from "react-router-dom";

import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core";

import { connect } from 'react-redux';
import {authLogin, authInit} from '../../../../ducks/auth';

import { ADMIN_DASHBOARD, AUTH_LOGIN } from "../../../../routes";

// Layout
import AuthLayoutFooter from "../Footer/Footer";
import AuthLayoutHeader from "../Header/Header";
import AuthLayoutMainCol from "./Col";

// Routes
import AuthLogin from "../../Login/Login";

// Styles
import AuthBck from "../../../../assets/images/authentication-bck.jpg";

const styles = theme => ({
  container: {
    backgroundColor: theme.colors.white
  },
  row: {
    display: "flex",
    width: "100%"
  },
  bck: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    height: "100%",
    backgroundImage: `linear-gradient(rgba(255, 145, 0, .25), rgba(255, 145, 0, .25)), url(${AuthBck})`,
    backgroundRepeat: "no-repeat",
    backgroundAttachment: "fixed",
    backgroundPosition: "center center",
    backgroundSize: "cover"
  },
  column: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100%"
  },
  content: {
    flex: "1 0 auto",
    paddingTop: 5 * theme.spacing.unit,
    width: "100%"
  },
  auth: {
    paddingTop: 8 * theme.spacing.unit
  },
  white: {
    textAlign: "center",
    color: theme.colors.white
  }
});
class AuthLayoutMain extends Component{

  componentDidMount() {
    this.props.authInit();
  }

  onLoginSubmit = (data) => {
    this.props.authLogin(data);
  }

  render() {
    if ( this.props.token ) {
      return <Redirect to={ADMIN_DASHBOARD} />;
    }
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <main className={classes.row}>
          <AuthLayoutMainCol>
            <div className={classes.bck}>
              <div>
                <Typography
                  component="h1"
                  variant="h4"
                  className={classes.white}
                  gutterBottom
                >
                  Plan your blogial
                </Typography>
                <Typography
                  variant="body1"
                  className={classes.white}
                  gutterBottom
                >
                  Un mélange of something que tu aimes
                </Typography>
              </div>
            </div>
          </AuthLayoutMainCol>
          <AuthLayoutMainCol>
            <div className={classes.column}>
              <div className={classes.content}>
                <AuthLayoutHeader />
                <div className={classes.auth}>
                  <Route 
                  path={AUTH_LOGIN} 
                  render={() => <AuthLogin onSubmit={this.onLoginSubmit} />}  />
                </div>
              </div>
              <AuthLayoutFooter />
            </div>
          </AuthLayoutMainCol>
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error,
    loading: state.auth.loading,
    token: state.auth.token,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    authInit: ( ) => dispatch(authInit ()),
    authLogin: ( data ) => dispatch(authLogin (data))
  };
};

export default connect( mapStateToProps, mapDispatchToProps )(withStyles(styles)(AuthLayoutMain));
