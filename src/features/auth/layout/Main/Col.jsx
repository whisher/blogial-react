import React from "react";

// Styles
import { withStyles } from "@material-ui/core";

const styles = () => ({
  root: {
    minHeight: "100vh",
    flex: "0 0 50%",
    maxWidth: "50%"
  }
});

const AuthLayoutMainCol = ({ classes, children }) => {
  return <div className={classes.root}>{children}</div>;
};

export default withStyles(styles)(AuthLayoutMainCol);
