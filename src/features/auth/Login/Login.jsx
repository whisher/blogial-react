import React, { Component } from "react";

import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import MailIcon from "@material-ui/icons/Mail";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import { withStyles } from "@material-ui/core";

import { isValid, isEmail } from "../../../utils/validators";
import Alert from '../../../shared/ui/Alert/Alert';
import LoaderButton from '../../../shared/ui/LoaderButton/LoaderButton';

const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column"
  },
  formControl: {
    marginBottom: 3 * theme.spacing.unit
  },
  input: {
    display: "block",
    width: "100%"
  },
  button: {
    display: "block",
    width: "100%",
    color: theme.colors.white
  }
});

class AuthLogin extends Component {
  state = {
    data:{
      email: "",
      password: ""
    },
    showPassword: false
  };
  
  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleChange = name => event => {
    this.setState({
      data:{
        ...this.state.data,
        [name]: event.target.value
      }
    });
  };

  isValidForm() {
    return !(
      isValid(this.state.data.email) &&
      isValid(this.state.data.password) &&
      isEmail(this.state.data.email)
    );
  }
  
  submitHandler = ( event ) => {
    event.preventDefault();
    this.props.onSubmit(this.state.data);
  }

  render() {  
    const { classes } = this.props;
    const error = this.props.error && <Alert>Invalid email or password</Alert>;
    return (
      <React.Fragment>
        <Grid container justify="center">
          <Grid item xs={12} sm={10} lg={8}>
            <form onSubmit={this.submitHandler} noValidate autoComplete="off">
              {error}
              <div className={classes.container}>
                <FormControl className={classes.formControl}>
                  <InputLabel required htmlFor="auth-email">
                    Email
                  </InputLabel>
                  <Input
                    id="auth-email"
                    startAdornment={
                      <InputAdornment position="start">
                        <MailIcon />
                      </InputAdornment>
                    }
                    value={this.state.email}
                    onChange={this.handleChange("email")}
                  />
                </FormControl>
                <FormControl className={classes.formControl}>
                  <InputLabel required htmlFor="auth-password">
                    Password
                  </InputLabel>
                  <Input
                    id="auth-password"
                    type={this.state.showPassword ? "text" : "password"}
                    value={this.state.password}
                    onChange={this.handleChange("password")}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={this.handleClickShowPassword}
                        >
                          {this.state.showPassword ? (
                            <VisibilityOff />
                          ) : (
                            <Visibility />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
                <FormControl> 
                  <LoaderButton 
                    type="submit" 
                    variant="contained"
                    color="primary" 
                    loading={this.props.loading}
                    disabled={this.isValidForm()}
                  >
                    Login
                  </LoaderButton>
                </FormControl>
              </div>
            </form>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(AuthLogin);
