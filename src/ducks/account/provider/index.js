import axios from "../../../axios";
import API_URLS from "../../../constants/api";

const accountUrl = API_URLS.account;

const getAccount = () => {
  return axios.get(accountUrl).then(response => {
    return response.data;
  });
};

export const Provider = {
  getAccount
};
