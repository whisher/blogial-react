import * as actionTypes from "../actions/actionTypes";

const initialState = {
  account: null,
  error: null,
  loaded: false,
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ACCOUNT_START:
      return {
        ...state,
        loading: true
      };
    case actionTypes.ACCOUNT_FAIL: {
      const error = action.payload.error;
      return {
        ...state,
        loading: false,
        error
      };
    }
    case actionTypes.ACCOUNT_SUCCESS: {
      const account = action.payload.account;
      console.log("account", account);
      return {
        ...state,
        error: null,
        loading: false,
        loaded: true,
        account
      };
    }
    default:
      return state;
  }
};

export default reducer;
