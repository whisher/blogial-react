import * as actionTypes from "./actionTypes";
import { Provider } from "../provider";

export const accountStart = () => {
  return {
    type: actionTypes.ACCOUNT_START
  };
};

export const accountSuccess = account => {
  return {
    type: actionTypes.ACCOUNT_SUCCESS,
    payload: { account }
  };
};

export const accountFail = error => {
  return {
    type: actionTypes.ACCOUNT_FAIL,
    payload: { error: error }
  };
};

export const getAccount = () => {
  return dispatch => {
    dispatch(accountStart());
    Provider.getAccount()
      .then(data => {
        dispatch(accountSuccess(data));
      })
      .catch(err => {
        dispatch(accountFail(err));
      });
  };
};
