import * as actionTypes from "./actionTypes";
import { Provider } from "../provider";
import { Storage } from "../../../utils/storage";

export const authInit = () => {
  return dispatch => {
    const token = Storage.local.get("auth");
    console.log(token);
    if (!token) {
      dispatch(authLogout());
    } else {
      const now = new Date();
      const expiredAt = new Date();
      expiredAt.setSeconds(expiredAt.getSeconds() + token.expiresIn);
      const isValidToken = expiredAt > now;
      if (!isValidToken) {
        dispatch(authLogout());
      } else {
        dispatch(authLoginSuccess(token));
      }
    }
  };
};

export const authLoginStart = () => {
  return {
    type: actionTypes.AUTH_LOGIN_START
  };
};

export const authLoginSuccess = token => {
  return {
    type: actionTypes.AUTH_LOGIN_SUCCESS,
    payload: { token }
  };
};

export const authLoginFail = error => {
  return {
    type: actionTypes.AUTH_LOGIN_FAIL,
    payload: { error: error }
  };
};

export const authLogout = () => {
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const authLogin = data => {
  return dispatch => {
    dispatch(authLoginStart());
    Provider.login(data)
      .then(data => {
        Storage.local.set("auth", data);
        dispatch(authLoginSuccess(data));
      })
      .catch(err => {
        dispatch(authLoginFail(err));
      });
  };
};
