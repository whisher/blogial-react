import axios from "../../../axios";
import API_URLS from "../../../constants/api";

const loginUrl = API_URLS.auth.login;

const login = data => {
  return axios.post(loginUrl, data).then(response => {
    return response.data;
  });
};

export const Provider = {
  login
};
