import * as actionTypes from "../actions/actionTypes";

const initialState = {
  error: null,
  loading: false,
  token: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_LOGIN_START:
      return {
        ...state,
        loading: true
      };
    case actionTypes.AUTH_LOGIN_FAIL: {
      const error = action.payload.error;
      return {
        ...state,
        loading: false,
        error
      };
    }
    case actionTypes.AUTH_LOGIN_SUCCESS: {
      const token = action.payload.token;
      return {
        ...state,
        error: null,
        loading: false,
        token
      };
    }
    case actionTypes.AUTH_LOGOUT:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
