const SESSION = "session";
const LOCAL = "local";

const StorageType = {
  SESSION,
  LOCAL
};

const getStorage = type => {
  if (type === StorageType.SESSION) {
    return window.sessionStorage;
  }
  return window.localStorage;
};

const setItem = type => (key, value) => {
  getStorage(type).setItem(key, JSON.stringify(value));
};

const getItem = type => key => {
  const val = getStorage(type).getItem(key);
  if (!val) return null;
  try {
    return JSON.parse(val);
  } catch (e) {
    return val;
  }
};

const removeItem = type => key => {
  getStorage(type).removeItem(key);
};

export const Storage = {
  session: {
    get: getItem(StorageType.SESSION),
    remove: removeItem(StorageType.SESSION),
    set: setItem(StorageType.SESSION)
  },
  local: {
    get: getItem(StorageType.LOCAL),
    remove: removeItem(StorageType.LOCAL),
    set: setItem(StorageType.LOCAL)
  }
};
