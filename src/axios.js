import axios from "axios";

const instance = axios.create({
  baseURL: process.env.API_URL,
  timeout: 3000
});

export default instance;
