// Core
import React from "react";

//import Loadable from 'loadable-components';

// Router
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { HOME, AUTH, ADMIN } from "./routes";

// Theme
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

// App
import { AuthLayoutMain } from "./features/auth/layout";
import { BlogLayoutMain } from "./features/blog/layout";
import { AdminLayoutMain } from "./features/admin/layout";

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route path={AUTH} component={AuthLayoutMain} />
          <Route path={ADMIN} component={AdminLayoutMain} />
          <Route path={HOME} exact component={BlogLayoutMain} />
          <Redirect from="*" to={HOME} />
        </Switch>
      </Router>
    </MuiThemeProvider>
  );
};

export default App;
