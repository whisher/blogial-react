// Core
import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import configureStore from './configureStore';

import { authLogout } from './ducks/auth';
import setupAxiosInterceptors from './axios-interceptor';

const store = configureStore();

const {dispatch} = store;
setupAxiosInterceptors(()=>{
  dispatch(authLogout())
});

// Css
import "./assets/styles/reset.css";
import "./assets/styles/app.css";

// App
const MOUNT_POINT = "root";
import App from "./App";

const app = (
  <Provider store={store}>
      <App />
  </Provider>
);
ReactDOM.render(app, document.getElementById(MOUNT_POINT));
