import axios from "./axios";
import { Storage } from "./utils/storage";

const setupAxiosInterceptors = onUnauthenticated => {
  const onRequestSuccess = config => {
    console.log("request success", config);
    const token = Storage.local.get("auth");
    if (token) {
      config.headers.Authorization = `${token.token}`;
    }
    return config;
  };
  const onRequestFail = error => {
    console.log("request error", error);
    return Promise.reject(error);
  };
  axios.interceptors.request.use(onRequestSuccess, onRequestFail);

  const onResponseSuccess = response => {
    console.log("response success", response);
    return response;
  };
  const onResponseFail = error => {
    console.log("response error", error);
    const status = error.status || error.response.status;
    if ((status === 403 || status === 401) && status === 204) {
      onUnauthenticated();
    }
    return Promise.reject(error);
  };
  axios.interceptors.response.use(onResponseSuccess, onResponseFail);
};
export default setupAxiosInterceptors;
