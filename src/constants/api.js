const API_URLS = {
  auth: {
    login: "api/users/login"
  },
  account: "api/account"
};
export default API_URLS;
