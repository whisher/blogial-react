export const HOME = `/`;

export const AUTH = `/auth`;
export const AUTH_LOGIN = `/auth/login`;

export const ADMIN = `/admin`;
export const ADMIN_DASHBOARD = `/admin/dashboard`;
export const ADMIN_POSTS = `/admin/posts`;
export const ADMIN_POSTS_POST = `/admin/posts/post`;
export const ADMIN_POSTS_POST_ID = `/admin/posts/post/:id`;
