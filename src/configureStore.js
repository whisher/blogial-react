// Redux
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";

// Ducks
import { authReducer } from "./ducks/auth";
import { accountReducer } from "./ducks/account";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  auth: authReducer,
  account: accountReducer
});
const loggerMiddleware = createLogger();
let middleware = [thunkMiddleware];

if (process.env.NODE_ENV === "development") {
  const freeze = require("redux-freeze");
  middleware = [...middleware, loggerMiddleware, freeze];
}

export default function configureStore() {
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware))
  );
  return store;
}
