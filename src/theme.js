import { createMuiTheme } from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ff9100"
    },
    secondary: {
      main: "#43a047"
    },
    error: {
      main: red[600]
    },
    text: {
      disabled: "#757575",
      hint: "#37474f",
      primary: "#424242",
      secondary: "#9e9e9e"
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: [
      "Roboto",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    htmlFontSize: 10,
    fontSize: 12
  },
  colors: {
    dark: "#aaaaaa",
    darker: "rgba(48,58,71,.87)",
    gray: "#f0f0f0",
    light: "rgb(242,242,242,.4)",
    orange: "#f2994a",
    purple: "#bb6bd9",
    red: "#d3292c",
    white: "#ffffff",
    yellow: "#f2c94c"
  },
  layout: {
    admin: {
      container: {
        width: 1140
      },
      masthead: {
        height: 85
      },
      nav: {
        height: 60
      }
    }
  }
});

export default theme;
